/*
   Nick Muenchen
   CPSC 1020 Section 1, Sp17
   nmuench
*/
#ifndef HEADER_H
#define HEADER_H
#include "Point.h"
using namespace std;


class Header
{
  private:
			string magNum;
			int width, height, maxVal;

  public:
			void setMagic(string);
			void setWidth(int);
			void setHeight(int);
			void setMax(int);
			void printHead(ofstream &);
			int getHeight();
			int getWidth();
    
};

#endif
