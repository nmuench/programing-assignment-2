/* Nick Muenchen
	CPSC 1020 Section 1, Sp17
	nmuench
*/
#include "Image.h"
using namespace std;

//Prototype for helper functions
bool pointInTriangle(Point checkPoint, Point tri1, Point tri2, Point tri3);
void makeSecondTriangle(Point p1, Point p2, Point p3, Image * check);

int main(int argc, char *argv[])
{
	ofstream outPut(argv[2]);
	ifstream inPut(argv[1]);
	bool checkTri;
	double coordX, coordY;
	int i, numPixels;
/*	
	Create an instance of the Image class and Point to represent 
   the points of the triangles.
*/
	Image newImage;
	Point pixCoord, triOne, triTwo, triThree, tri2One, tri2Two, tri2Three;    

	if(argc != 3)
	{
		cout << "USAGE ERROR:  ./executable outPutFileName";
		exit(EXIT_FAILURE);
	}
	if(inPut.fail())
	{
		cout << argv[1] << " did not open successfully\n";
	}

	if(outPut.fail())
  	{
		cout << argv[2] << " did not open successfully\n";
	}
	//Creates the header using the values in the input file
	newImage.setHeader(inPut);
	//It then creates the array of pixels.
	newImage.makePixels();
	//This prints the header to the output file.
	newImage.printHeader(outPut);
	//Scans in the three points that make the triangle.
	inPut >> coordX >> coordY;
	triOne.setBoth(coordX, coordY);
	inPut >> coordX >> coordY;
	triTwo.setBoth(coordX, coordY);
	inPut >> coordX >> coordY;
	triThree.setBoth(coordX, coordY);
	//Scans in the points of the second triangle
	inPut >> coordX >> coordY;	
	tri2One.setBoth(coordX, coordY);
	inPut >> coordX >> coordY;
	tri2Two.setBoth(coordX, coordY);
	inPut >> coordX >> coordY;
	tri2Three.setBoth(coordX, coordY);
	//Sets numPixels equal to the number of pixels in the image.
	numPixels = newImage.getNumPix();
	/*
		This loop goes through all of the pixels in the image
		and checks to see which are in the triangle. If a 
		pixel is in the triangle, it uses the setRGB function
		to give it the color of the pixels in the triangle.
		If not, it uses the same function to give it the color
		of pixels that are not in the triangle.
	*/
	for(i = 0; i < numPixels; i++)
	{
		pixCoord = newImage.getPixPoint(i);
		checkTri = pointInTriangle(pixCoord, triOne, triTwo, triThree);
		newImage.setRGB(checkTri, i);
	}
	//Calls the helper function to create the second triangle
	makeSecondTriangle(tri2One, tri2Two, tri2Three, &newImage);
	//Prints out the image to the output file.
	newImage.printColors(outPut);
	
	inPut.close();
	outPut.close();

	return 0;
}
/*
	This program takes in four points and determines if
	the first point is inside of a triangle made by connecting
	the other three points. It returns true if it is inside this
	triangle and false if it is not.
*/
bool pointInTriangle(Point checkPoint, Point tri1, Point tri2, Point tri3)
{
	double x1 = tri1.getX(), y1 = tri1.getY();
	double x2 = tri2.getX(), y2 = tri2.getY();
	double x3 = tri3.getX(), y3 = tri3.getY();
	double xCheck = checkPoint.getX(), yCheck = checkPoint.getY();
	double denominator, a, b, c;
	denominator = (((y2 - y3) * (x1 - x3)) + ((x3 - x2) * (y1 - y3)));
	a = ((y2 - y3) * (xCheck - x3)) + ((x3 - x2) * (yCheck - y3));
	a = a / denominator;
	b = ((y3 - y1) * (xCheck - x3)) + ((x1 - x3) * (yCheck - y3));
	b = b / denominator;
	c = 1 - a - b;
	if(0 <= a && a <= 1)
	{
		if(0 <= b && b <= 1)
		{
			if(0 <= c && c <= 1)
			{
				return true;
			}
		}
	}
	else
	{
		return false;
	}
	return false;
}
//This function makes the overlapping triangle
void makeSecondTriangle(Point p1, Point p2, Point p3, Image * check)
{
	Point testPoint;
	int i;
	bool isIn = true;
	
	for(i = 0; i < check->getNumPix(); i++)
	{
		testPoint = check->getPixPoint(i);
		isIn = pointInTriangle(testPoint, p1, p2, p3);
		if(isIn)
		{
			check->setRGB(255, 255, 255, i);
		}
	}
	return;
}


