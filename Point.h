/*
   Nick Muenchen
   CPSC 1020 Section 1, Sp17
   nmuench
*/
#ifndef POINT_H
#define POINT_H
#include <fstream>
#include <string>
#include <iostream>
#include <stdlib.h>
#include <vector>
class Point
{
    private:
				double x;
				double y;

    public:
    			void setBoth(double, double);
				void setX(double);
				void setY(double);
				double getX();
				double getY();
};
#endif
