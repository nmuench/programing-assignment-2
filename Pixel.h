/*
   Nick Muenchen
   CPSC 1020 Section 1, Sp17
   nmuench
*/
#ifndef PIXEL_H
#define PIXEL_H
#include "Color.h"
#include "Point.h"
using namespace std;


class Pixel
{
  private:
   			Color col;
				Point place;
  public:
				void setColor(bool);
				void setColor(unsigned char, unsigned char, unsigned char);
				void printPix(ofstream &);
				void setPoint(double, double);
				Point getPlace();
    

};
#endif
