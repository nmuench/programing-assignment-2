/* Nick Muenchen
	CPSC 1020 Section 1, Sp17
	nmuench
*/
#include "Point.h"
//Sets the values of x and y to those given in the parameters.
void Point::setBoth(double xVal, double yVal)
{
	x = xVal;
	y = yVal;
	return;
}
//Sets the value of x to that given in the paramters.
void Point::setX(double xVal)
{
	x = xVal;
	return;
}
//Sets the value of y to that given in the paramters.
void Point::setY(double yVal)
{
	y = yVal;
	return;
}
//Returns the value of x.
double Point::getX()
{
	return x;
}
//Returns the value of y.
double Point::getY()
{
	return y;
}
