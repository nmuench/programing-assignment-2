/* Nick Muenchen
	CPSC 1020 Section 1, Sp17
	nmuench
*/

#include "Color.h"
/*
	Sets the three variables in the Color object to
	the values provided for them in the parameters.
*/
void Color::setAll(unsigned char r, unsigned char g, unsigned char b)
{
	red = r;
	green = g;
	blue = b;
	return;
}
//Sets the red value to the value given in the parameter.
void Color::setRed(unsigned char rVal)
{
	red = rVal;
	return;
}
//Sets the green value to the value given in the parameter.
void Color::setGreen(unsigned char gVal)
{
	green = gVal;
	return;
}
//Sets the blue variable to value given in the parameter.
void Color::setBlue(unsigned char bVal)
{
	blue = bVal;
	return;
}
//Returns the value of red.
unsigned char Color::getRed()
{
	return red;
}
//Returns the value of green.
unsigned char Color::getGreen()
{
	return green;
}
//Returns the value of blue.
unsigned char Color::getBlue()
{
	return blue;
}
