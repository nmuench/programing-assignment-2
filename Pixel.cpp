/* Nick Muenchen
	CPSC 1020 Section 1, Sp17
	nmuench
*/
#include "Pixel.h"
/*
	This functions uses the color class function setFull
	to set the color to one of two possibilities depending
	on whether the boolean passed to it is true or false.
*/
void Pixel::setColor(bool inTriangle)
{	
	unsigned char r1 = 234, g1 = 106, b1 = 32;
	unsigned char r2 = 82, g2 = 45, b2 = 128;
	if(inTriangle)
	{
		col.setAll(r1, g1, b1);
	}
	else
	{
		col.setAll(r2, g2, b2);
	}
	return;
}
/* 
	This function calls the color class functions setFull to set the
	value of the pixel's color to the RGB values given in the parameters.
	I am aware that its first line is over the allowed 80 character limit,
	but it was impossible to shorten it while keeping the specific variable
	names.
*/
void Pixel::setColor(unsigned char redVal, unsigned char greenVal, unsigned char blueVal)
{
	col.setAll(redVal, greenVal, blueVal);
	return;
}
/*
	This function prints the pixel to an output file
	given in the parameters.
*/
void Pixel::printPix(ofstream &  outPut)
{
	unsigned char redOut, greenOut, blueOut;
	redOut = col.getRed();
	greenOut = col.getGreen();
	blueOut = col.getBlue();
	outPut << redOut << greenOut << blueOut;
	return;
}
/*
	This functions sets the Pixels point 
	to the coordinates in theparameters
	by calling the setBoth functions of place.
*/
void Pixel::setPoint(double xCoord, double yCoord)
{
	place.setBoth(xCoord, yCoord);
	return;
}
/*
	This function returns the point of place.
*/
Point Pixel::getPlace()
{
	return place;
}

