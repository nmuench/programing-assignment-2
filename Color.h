/*
   Nick Muenchen
   CPSC 1020 Section 1, Sp17
   nmuench
*/
#ifndef COLOR_H
#define COLOR_H
#include "Point.h"
class Color
{
  private:
				unsigned char red;
				unsigned char green;
				unsigned char blue;

  public:
				void setAll(unsigned char, unsigned char, unsigned char);
				void setRed(unsigned char);
				void setGreen(unsigned char);
				void setBlue(unsigned char);
				unsigned char getRed();
				unsigned char getGreen();
				unsigned char getBlue();
};

#endif
