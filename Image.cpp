/* Nick Muenchen
	CPSC 1020 Section 1, Sp17
	nmuench
*/
#include "Image.h"
/*
	This function takes in a file pointer as
	a parameter. It then takes the values for
	the header from that file, calls the header
	setting functions, and creates the header.
*/
void Image::setHeader(ifstream & inPut)
{
	int widthNum, heightNum;
	inPut >> widthNum >> heightNum;
	hdr.setMagic("P6");
	hdr.setWidth(widthNum);
	hdr.setHeight(heightNum);
	hdr.setMax(255);
	return;
}
/*
	This function takes in a file pointer
	as a parameter, and then calls the header
	function printHead to print the header to
	this file.
*/
void Image::printHeader(ofstream & outPutFile)
{
	hdr.printHead(outPutFile);
	return;
}
/*
	This function calls the header
	functions getHeight and getWidth
	to find the total number of pixels in the
	image, and then returns that value.
*/
int Image::getNumPix()
{
	return hdr.getHeight() * hdr.getWidth();
}
/*
	This function calls on the Header
	functions getHeight and getWidth
	to determine the needed amount of
	points. It then resizes the pixel vector
	to that size, and calls the Pixel function
	setPoint to set each pixels point value.
*/
void Image::makePixels()
{
	int numPix, i, j, count = 0;
	numPix = hdr.getWidth();
	numPix *= hdr.getHeight();
	pix.resize(numPix);
	for(i = 0; i < hdr.getHeight(); i++)
	{
		for(j = 0; j < hdr.getWidth(); j++)
		{
			pix.at(count++).setPoint(static_cast<double>(j), static_cast<double>(i));
		}
	}
	return;
}
/*
	This function class the Pixel function
	setColor to set the color values of its pixel
	object. The first line goes over the character limit
	because the parameter names needed to be unique.
*/
void Image::setRGB(unsigned char rChar, unsigned char gChar, unsigned char bChar, int index)
{
	pix.at(index).setColor(rChar, gChar, bChar);
	return;
}
/*
	This overloaded function calls the Pixel function
	setColor to set the color value of its pixel based
	on a boolean value given in its parameter.
*/
void Image::setRGB(bool trianglePoint, int index)
{
	pix.at(index).setColor(trianglePoint);
	return;
}
/*
	This function calls the Pixel function
	getPlace to return the point of the
	pixel.
*/
Point Image::getPixPoint(int pixIndex)
{
	return pix.at(pixIndex).getPlace();
}
/*
	This function calls on the Pixel function
	printPix to print the pixel to the file
	passed to it in the parameters as a file pointer.
*/
void Image::printColors(ofstream & output)
{
	int i;
	for(i = 0; i < (hdr.getHeight() * hdr.getWidth()); i++)
	{
		pix.at(i).printPix(output);
	}
	return;
}


