/* Nick Muenchen
	CPSC 1020 Section 1, Sp17
	nmuench
*/
#include "Header.h"
//This function sets the magic number that given in the parameter
void Header::setMagic(string picNum)
{
	magNum = picNum;
	return;
}
//This function sets the width variable to the value of the parameter.
void Header::setWidth(int widthVal)
{
	width = widthVal;
	return;
}
//This function sets the height variable to the value of the parameter.
void Header::setHeight(int heightVal)
{
	height = heightVal;
	return;
}
//This function sets the max variable to the value in the parameter.
void Header::setMax(int maximum)
{
	maxVal = maximum;
	return;
}
/* 
	This functions prints out the header
	to the file given in the parameters.
*/
void Header::printHead(ofstream & outputFile)
{
	outputFile << magNum << endl;
	outputFile << width << " " << height << " ";
	outputFile << maxVal << endl;
	return;
}
/* 
	This function returns the width variable.
*/
int Header::getWidth()
{
	return width;
}
/*
	This function returns the height variable.
*/
int Header::getHeight()
{
	return height;
}

