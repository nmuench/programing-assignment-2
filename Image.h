/*
	Nick Muenchen
	CPSC 1020 Section 1, Sp17
	nmuench
*/
#ifndef IMAGE_H
#define IMAGE_H
#include "Pixel.h"
#include "Header.h"

class Image
{
  private:
			vector<Pixel> pix;
			Header hdr;

  public:
			void setHeader(ifstream&);
			void printHeader(ofstream&);
			void makePixels();
			int getNumPix();
			void setRGB(unsigned char, unsigned char, unsigned char, int);
			void setRGB(bool, int);
			Point getPixPoint(int);
			void printColors(ofstream&);
   

};
#endif
